//jQuery Button Demo
//Name: Matthew Nevish
//Date: Feb 16, 2017 - Revised on Feb 23, 2017

$(document).ready(function(){
    
    var wiseDiv = $("#wiseMonkeyFirst");
    var wiseDiv2 = $("#wiseMonkeySecond");
    var monkeyMessage = $("#message");
    var monkeyPic1;
    var monkeyPic2;
    
    $('button').each(function(){
        $(this).click(function(){
            if ((wiseDiv.css("background-image") === "none") && (wiseDiv2.css("background-image") === "none")){
                monkeyPic1 = $(this).find("img").attr("src");
                var url = "url(" + monkeyPic1 + ")";
                wiseDiv.css("background-image",url);
                monkeyMessage.text($(this).find("img").attr("alt"));
            }
            else if ((wiseDiv.css("background-image") != "none") && (wiseDiv2.css("background-image") === "none")){
                monkeyPic2 = $(this).find("img").attr("src");
                var url = "url(" + monkeyPic2 + ")";
                wiseDiv2.css("background-image",url);
                if(monkeyPic1 === monkeyPic2){
                    monkeyMessage.text($(this).find("img").attr("alt"));
                }
                else {
                    monkeyMessage.text("That was unwise");
                }
            }
            else if((wiseDiv.css("background-image") != "none") && (wiseDiv2.css("background-image") != "none")){
                monkeyPic1 = $(this).find("img").attr("src");
                var url = "url(" + monkeyPic1 + ")";
                wiseDiv.css("background-image",url);
                wiseDiv2.css("background-image","none");
                monkeyMessage.text("");
            }
        });
    });
            
});